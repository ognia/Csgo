'use strict';

var appControllers = angular.module('appControllers', []);
var appServices    = angular.module('appServices', ['ngResource']);

var app = angular.module('app', [
	'ngAnimate',
	'validation.match',
	'ngResource',
	'ngFileUpload',
	'LocalStorageModule',
	'appServices',
	'appControllers',
]);

app.config(['$qProvider', 'localStorageServiceProvider',
	function ($qProvider, localStorageServiceProvider) {
		$qProvider.errorOnUnhandledRejections(false);
		localStorageServiceProvider.setPrefix('csgo');
	}
]);
