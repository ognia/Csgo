'use strict';

appControllers.controller('StreamCtrl', ['$scope', 'StreamServices',
	function($scope, StreamServices) {
		var vm = this;

		vm.usersFilter = {};
		vm.PATH        = {};
		vm.usersSelect = [];

		vm.setPath     = setPath;
		vm.filterUser  = filterUser;
		vm.setFavorite = setFavorite;
		vm.selectUser  = selectUser;


		function setPath(data) {
			vm.usersFilter.favoriteOnly = data.favoriteOnly;
			vm.usersFilter.onlineOnly   = data.onlineOnly;
			vm.usersFilter.from         = data.from;
			vm.usersFilter.searchString = data.searchString;

			vm.PATH.addFavoritePath     = data.addFavoritePath;
			vm.PATH.removeFavoritePath  = data.removeFavoritePath;
			vm.PATH.getUsersPath        = data.getUsersPath;
			vm.PATH.getUserFavoritePath = data.getUserFavoritePath;
			vm.PATH.roomPath            = data.roomPath;

			vm.isLocal  = data.isLocal;

			vm.filterUser();
		}

		function selectUser($event, user) {
			$event.preventDefault();

			StreamServices.setSelectUser(user);
			vm.usersSelect = StreamServices.getSelectUser();
		}

		function setFavorite($event, user) {
			$event.preventDefault();
			var element    = $event.target;
			var params     = { username: user.Username };
			var isFavorite = $(element).hasClass('active');
			var action     = isFavorite ?
							 vm.PATH.removeFavoritePath :
							 vm.PATH.addFavoritePath;
			var setFavorite = StreamServices.getData(action, params);

			setFavorite.then(function(value) {
				if(!value) {
					return false;
				}

				if(vm.usersFilter.favoriteOnly && isFavorite) {
					var index = vm.users.indexOf(user);
					vm.users.splice(index, 1);
				}

				user.IsFavorite = !user.IsFavorite;
			});
		}

		function filterUser() {
			var action = vm.usersFilter.favoriteOnly ?
						 vm.PATH.getUserFavoritePath :
						 vm.PATH.getUsersPath;

			var getUser = StreamServices.getData(action, vm.usersFilter);

			getUser.then(function(value) {
				vm.users       = value.users;
				vm.totalCount  = value.totalCount;
				vm.onlineCount = value.onlineCount;
			});

			return false;
		}
	}
]);
