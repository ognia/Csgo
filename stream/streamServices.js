'use strict';

appServices.service('StreamServices', ['$http', '$q',
	function($http, $q) {
		return({
			getData       : getData,
			setSelectUser : setSelectUser,
			getSelectUser : getSelectUser,
			usersSelect   : []
		});

		var deferred = $q.defer();

		function setSelectUser(user) {
			if(angular.isUndefined(user.isSelect) || !user.isSelect) {
				user.isSelect = true;
				this.usersSelect.push(user);
			} else {
				var index = this.usersSelect.indexOf(user);

				user.isSelect = false;
				this.usersSelect.splice(index, 1);
			}
		}

		function getSelectUser() {
			return this.usersSelect;
		}

		function getData(action, params) {
			var request = $http({
				method : 'POST',
				url    : action,
				params : params
			});
			return(request.then(handleSuccess, handleError));
		}

		function handleError( response ) {
			if (!angular.isObject(response.data) ||
				!response.data.message) {
				return(deferred.reject("An unknown error occurred."));
			}
			// Otherwise, use expected error message.
			return(deferred.reject(response.data.message));
		}
		// I transform the successful response, unwrapping the application data
		// from the API response payload.
		function handleSuccess(response) {
			return(response.data);
		}
	}
]);
